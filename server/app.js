const express = require('express');
const app = express();
const morgan = require('morgan');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const recipeRoutes = require('./api/routes/recipes');
const categoryRoutes = require('./api/routes/categories');

mongoose.connect('mongodb://cookcook:123123@127.0.0.1/cooking').then(() => { 

console.log("Connected to Database"); 

}).catch((err) => { 

console.log("Not Connected to Database ERROR! ", err); 

}); 

let db = mongoose.connection; 

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use('/recipes', recipeRoutes);
app.use('/categories', categoryRoutes);

module.exports = app;