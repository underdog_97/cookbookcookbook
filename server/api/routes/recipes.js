const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const Recipe = require('../models/recipe');

const RecipesController = require('../controllers/recipes');

router.get('/', RecipesController.recipes_get_all);

// router.post('/', (req, res, next) => {
//     const recipe = new Recipe({
//         _id: new mongoose.Types.ObjectId(),
//         name: req.body.name,
//         text: req.body.text
//     })
//     recipe
//         .save()
//         .then(result => {
//             console.log(result)
//         })
//         .catch(err => console.log(err));
//     res.status(201).json({
//         message: 'Handling POST requests to /recipes',
//         recipe: recipe
//     })
// })

// router.get('/:recipeId', (req, res, next) => {
//     const id = req.params.recipeId;
//     res.status(200).json({
//         message: id
//     })
// })

// router.patch('/:recipeId', (req, res, next) => {
//     const id = req.params.recipeId;
//     res.status(200).json({
//         message: "patched" + id
//     })
// })

// router.delete('/:recipeId', (req, res, next) => {
//     const id = req.params.recipeId;
//     res.status(200).json({
//         message: "deleted" + id
//     })
// })

module.exports = router;