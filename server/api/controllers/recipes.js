const Recipe = require('../models/recipe');

exports.recipes_get_all = (req, res, next) => {
    Recipe.find()
        .exec()
        .then(docs => {
            console.log(docs);
            res.status(200).json(docs)
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({error: err});
        })
}