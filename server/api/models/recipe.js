const mongoose = require('mongoose');

const recipeSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: String,
    text: String,
})

module.exports = mongoose.model('Recipe', recipeSchema);